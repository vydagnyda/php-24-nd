<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\CarUpdateRequest;
use App\Car;
use App\Owner;

class CarController extends Controller
{
    public function list($id, Request $request)
    {
        $car = Car::find($id);
        $owner_id = $car->id;
        return view('car_list', [
            'car' => Car::find($id) 
            //,   'car_owner' => Owner::where('car_id','=','$owner_id')
            ]);            
    }

    public function edit($id, Request $request)
    {
        return view('car_edit', ['car' => Car::find($id)]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'reg_number' => 'required|max:6',
            'model' => 'required|max:32',
            'brand' => 'required|max:32',
        ]);
    }
}
