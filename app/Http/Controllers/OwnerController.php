<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Owner;

class OwnerController extends Controller
{
    public function list(Request $request)
    {
        return view('owner_list', [
            'owners' => Owner::all()
            ]);
    }
}
