@foreach ($owners as $owner)
    <h3>{{$owner->name}}   {{$owner->surname}}</h3>
    @foreach ($owner->cars() as $car)
    <ul>
        <li> {{ $car->reg_number }}</li>
        <li> {{ $car->brand }}</li>
        <li> {{ $car->model }}</li>
    </ul>

    @endforeach
@endforeach